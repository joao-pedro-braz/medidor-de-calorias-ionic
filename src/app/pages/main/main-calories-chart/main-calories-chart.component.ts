import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective, Label } from 'ng2-charts';
import { ICalorie } from '../../../core/interfaces/calory';
import { isToday } from '../../../core/helpers/isToday.helper';

@Component({
  selector: 'app-main-calories-chart',
  templateUrl: './main-calories-chart.component.html',
  styleUrls: ['./main-calories-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainCaloriesChartComponent implements OnInit {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  @Input('calories') set _calories(calories: ICalorie[]) {
    this.calories = calories;
    this.cdRef.detectChanges();
  }

  public calories: ICalorie[] = [];

  public options: ChartOptions = {
    responsive: true,
  };

  public labels: Label[] = [
    '00',
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
  ];

  public type: ChartType = 'bar';

  public get data(): ChartDataSets[] {
    return [{ data: this.aggregatedCalories, label: 'Calorias' }];
  }

  private get aggregatedCalories(): number[] {
    const caloriesFromToday = this.calories
      .filter((calorie) => isToday(new Date(calorie.date)))
      .reduce<Record<number, number>>((prev, curr) => {
        const date = new Date(curr.date);
        return {
          ...prev,
          ...{
            [date.getHours()]: (prev[date.getHours()] ?? 0) + curr.value,
          },
        };
      }, {});
    return Array.from({ length: 24 }).map(
      (_, index) => caloriesFromToday[index] ?? 0
    );
  }

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnInit() {}
}
