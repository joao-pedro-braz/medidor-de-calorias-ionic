import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainPageRoutingModule } from './main-routing.module';

import { MainPage } from './main.page';
import { MainCaloriesChartComponent } from './main-calories-chart/main-calories-chart.component';
import { ChartsModule } from 'ng2-charts';
import { MainCaloriesTableComponent } from './main-calories-table/main-calories-table.component';
import { MainCaloriesFormComponent } from './main-calories-form/main-calories-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainPageRoutingModule,
    ChartsModule,
  ],
  declarations: [
    MainPage,
    MainCaloriesChartComponent,
    MainCaloriesTableComponent,
    MainCaloriesFormComponent,
  ],
})
export class MainPageModule {}
