import { Component, OnInit } from '@angular/core';
import { ICalorie } from '../../../core/interfaces/calory';
import { StorageService } from '../../../core/services/storage.service';
import { ModalController, ToastController } from '@ionic/angular';
import { globals } from '../../../../globals';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { EMPTY, from, of } from 'rxjs';

@Component({
  selector: 'app-main-calories-form',
  templateUrl: './main-calories-form.component.html',
  styleUrls: ['./main-calories-form.component.scss'],
})
export class MainCaloriesFormComponent implements OnInit {
  public model: ICalorie = {
    name: null,
    date: new Date().toISOString(),
    value: null,
  };

  constructor(
    private storageService: StorageService,
    private modalController: ModalController,
    private toastController: ToastController
  ) {}

  ngOnInit() {}

  public addCalorie(): void {
    this.storageService
      .get(globals.caloriesKey)
      .pipe(
        map((calories) => calories ?? []),
        mergeMap((calories) =>
          this.storageService.set(globals.caloriesKey, [
            ...calories,
            {
              ...this.model,
              date: new Date(this.model.date),
            },
          ])
        )
      )
      .subscribe(() =>
        from(this.modalController.dismiss(true))
          .pipe(
            mergeMap(() =>
              from(
                this.toastController.create({
                  message: 'Caloria cadastrada com sucesso',
                  color: 'success',
                  duration: 1000,
                })
              )
            )
          )
          .subscribe((toast) => toast.present())
      );
  }

  public close(): void {
    this.modalController.dismiss(false);
  }
}
