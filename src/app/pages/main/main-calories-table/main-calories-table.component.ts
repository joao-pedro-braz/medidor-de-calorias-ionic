import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ICalorie } from '../../../core/interfaces/calory';
import { StorageService } from '../../../core/services/storage.service';
import { globals } from '../../../../globals';

@Component({
  selector: 'app-main-calories-table',
  templateUrl: './main-calories-table.component.html',
  styleUrls: ['./main-calories-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MainCaloriesTableComponent implements OnInit {
  @Input('calories') set _calories(calories: ICalorie[]) {
    this.calories = calories;
    this.cdRef.detectChanges();
  }

  @Output() delete = new EventEmitter<number>();

  public calories: ICalorie[] = [];

  constructor(
    private cdRef: ChangeDetectorRef,
    private storageService: StorageService
  ) {}

  ngOnInit() {}

  public headerFn(record: ICalorie, recordIndex: number) {
    const date = new Date(record.date);

    if (
      recordIndex === 0 ||
      date.getDate() !== new Date(this.calories[recordIndex - 1].date).getDate()
    ) {
      // This assumes the items are sorted by date
      // If we're here, this record is the first of it's day
      return `${date.getDate().toString().padStart(2, '0')}/${(
        date.getMonth() + 1
      )
        .toString()
        .padStart(2, '0')}/${date.getUTCFullYear()}`;
    }

    return null;
  }
}
