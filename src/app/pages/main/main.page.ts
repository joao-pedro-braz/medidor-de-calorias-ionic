import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../core/services/storage.service';
import {
  AlertController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { from } from 'rxjs';
import { MainCaloriesFormComponent } from './main-calories-form/main-calories-form.component';
import { catchError, filter, map, mergeMap } from 'rxjs/operators';
import { ICalorie } from '../../core/interfaces/calory';
import { globals } from '../../../globals';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  public calories: ICalorie[] = [];

  constructor(
    private storageService: StorageService,
    private modalController: ModalController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.loadData();
  }

  ionWillEnter(): void {
    this.loadData();
  }

  public showAddCalorieAlert(): void {
    from(
      this.modalController.create({
        component: MainCaloriesFormComponent,
      })
    ).subscribe((modal) => {
      modal.present();
      from(modal.onWillDismiss())
        .pipe(filter((data) => !!data))
        .subscribe(() => this.loadData());
    });
  }

  public delete(index: number): void {
    this.storageService
      .set(
        globals.caloriesKey,
        this.calories.filter((_, myIndex) => myIndex !== index)
      )
      .pipe(
        mergeMap(() =>
          from(
            this.toastController.create({
              message: 'Caloria removida com sucesso',
              color: 'success',
              duration: 1000,
            })
          )
        )
      )
      .subscribe((toast) => toast.present() && this.loadData());
  }

  private loadData(): void {
    this.storageService
      .get(globals.caloriesKey)
      .pipe(map((calories) => calories ?? []))
      .subscribe(
        (calories) =>
          (this.calories = calories.sort(
            (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
          ))
      );
  }
}
