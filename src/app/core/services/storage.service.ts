import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { filter, mergeMap, share } from 'rxjs/operators';
import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private storage$: BehaviorSubject<Storage> = new BehaviorSubject<Storage>(
    null
  );

  constructor(private storage: Storage) {
    this.init();
  }

  public set(key: string, value: any) {
    const op$ = this.storage$.pipe(
      filter((storage) => !!storage),
      mergeMap((storage) => from(storage.set(key, value))),
      share()
    );
    op$.subscribe();
    return op$;
  }

  public get(key: string) {
    const op$ = this.storage$.pipe(
      filter((storage) => !!storage),
      mergeMap((storage) => from(storage.get(key))),
      share()
    );
    op$.subscribe();
    return op$;
  }

  public remove(key: string) {
    const op$ = this.storage$.pipe(
      filter((storage) => !!storage),
      mergeMap((storage) => from(storage.remove(key))),
      share()
    );
    op$.subscribe();
    return op$;
  }

  private init() {
    from(this.storage.create() as Promise<Storage>).subscribe((storage) =>
      this.storage$.next(storage)
    );
  }
}
