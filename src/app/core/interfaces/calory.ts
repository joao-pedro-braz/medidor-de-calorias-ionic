export interface ICalorie {
  name: string;
  value: number;
  date: string | Date;
}
