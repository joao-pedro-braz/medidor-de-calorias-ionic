import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.idjoca.calories',
  appName: 'Medidor de Calorias',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
